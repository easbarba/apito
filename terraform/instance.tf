# Ruokin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ruokin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ruokin. If not, see <https://www.gnu.org/licenses/>.


resource "aws_instance" "ruokin" {
  ami           = var.ami_id
  instance_type = "t2.micro"
  key_name      = var.ami_key_pair_name
  security_groups = [
    "ruokin-sg-ssh",
    "ruokin-sg-ports-server",
    "ruokin-sg-http",
    "ruokin-sg-https"
  ]

  tags = {
    Name = "ruokin-project"
  }
}
