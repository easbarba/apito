# Ruokin

Evaluate soccer referees' performance.

## Stack

| Framework | Where                                      |
|-----------|--------------------------------------------|
| Vapor     | https://gitlab.com/easbarba/ruokin-vapor   |
| Vue.js    | https://gitlab.com/easbarba/ruokin-vue     |
| Quarkus   | https://gitlab.com/easbarba/ruokin-quarkus |

## Techs

- [Terraform](https://www.terraform.io)
- [Ansible Playbook](https://www.ansible.com)
- [AWS EC2](https://aws.amazon.com/ec2)
- [Koto](https://gitlab.com/easbarba/koto)
- [PostgreSQL](https://www.postgresql.org)
- [Vuejs](https://vuejs.org)
- [Podman](https://podman.io)
- [NGNIX](https://nginx.org)
- [Gitlab CI](https://gitlab.com)
- [Github Actions](https://github.com/features/actions)
- [Git](https://git-scm.com)
- [Debian](https://www.debian.org)
- [GNU](https://www.gnu.org) { [Guix](https://guix.gnu.org), [Emacs](https://www.gnu.org/software/emacs), [Make](https://www.gnu.org/software/make), [Bash](https://www.gnu.org/software/bash), [Coreutils](https://www.gnu.org/software/coreutils), [Guile](https://www.gnu.org/software/guile), ... }

Person CRUD using Node, React, MongoDB, Redis, RabbitMQ, Docker, Kubernetes and Tilt

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
